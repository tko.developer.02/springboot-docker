FROM openjdk:8-jdk-alpine
WORKDIR '/server'
ARG JAR
COPY ${JAR} app.jar
ENTRYPOINT ["java","-jar","app.jar"]